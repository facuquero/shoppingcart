package productApi.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import productApi.springboot.model.Transaction;
import productApi.springboot.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	ProductService productService;

	@Autowired
	TransactionRepository transactionRepository;

	public List<Transaction> getAllProducts() {
		return transactionRepository.findAll();
	}

	public Optional<Transaction> findById(Long id) {
		return transactionRepository.findById(id);
	}

	public void save(Transaction transaction) {
		transactionRepository.save(transaction);
	}

	public void update(Transaction transaction) {
		transactionRepository.save(transaction);
	}

	public void deleteById(Long id) {
		transactionRepository.deleteById(id);
		;
	}

	public List<Transaction> findAllByCode(String code) {
		return transactionRepository.findAllByCode(code);
	}

	public List<Transaction> findTransactionByCode(String code) {
		return transactionRepository.findAllByCode(code);

	}

}
