package productApi.springboot.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import productApi.springboot.model.Client;
import productApi.springboot.model.Product;
import productApi.springboot.model.Transaction;
import productApi.springboot.repository.ProductRepository;
import productApi.springboot.vo.ShoppingCart;

import java.util.UUID;

@Service
public class ProductService {

	ShoppingCart shopping = new ShoppingCart();

	@Autowired
	ProductRepository productRepository;
	@Autowired
	ProductService productService;
	@Autowired
	TransactionService transactionService;
	@Autowired
	ClientService clientService;

	public List<Product> getAllProducts() {
		return productRepository.findAll();
	}

	public Optional<Product> findById(Long id) {
		return productRepository.findById(id);
	}

	public void save(Product product) {
		productRepository.save(product);
	}

	public void update(Product product) {
		productRepository.save(product);
	}

	public void deleteById(Long id) {
		productRepository.deleteById(id);
		;
	}

	public String getShoppingCart() {
		return "Tu carrito de compra contiene " + shopping.getProducts().toString()
				+ " y es por el total a pagar es de " + shopping.getTotalPrice();
	}

	public String addShoppingCart(Long id) {
		Optional<Product> p = productRepository.findById(id);
		return shopping.addProducts(p.get());
	}

	public String deleteShoppingCart(int i) {
		return shopping.deleteProduct(i);

	}

	public String buy(String id) {

		Optional<Client> client = clientService.findById(id);
		if (!client.isPresent()) {
			return "Debe registrarse para hacer la compra";
		} else if (productService.getShoppingCart().isEmpty()) {
			return "No hay nada en el carrito";
		} else {
			String code = UUID.randomUUID().toString();
			for (Product p : shopping.getProducts()) {
				Transaction t = new Transaction();
				t.setProductId(p.getId());
				t.setProductName(p.getName());
				t.setUnitPrice(p.getPrice());
				t.setCode(code);
				t.setBillingDate(new Date());
				t.setClientId(client.get().getId());
				transactionService.save(t);
			}
			List<Transaction> transactionList = transactionService.findAllByCode(code);
			for (Transaction t2 : transactionList) {
				t2.setTotal(shopping.getTotalPrice());
				transactionService.update(t2);
			}
			shopping.emptyShopping();
			shopping.setTotalPrice(0);
			return "Su compra se ha realizado con exito, su codigo de compra es: " + code;
		}

	}
}
