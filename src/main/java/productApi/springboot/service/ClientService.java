package productApi.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import productApi.springboot.model.Client;
import productApi.springboot.repository.ClientRepository;

@Service
public class ClientService {

	@Autowired
	ClientRepository clientRepository;

	public List<Client> getAllClients() {
		return clientRepository.findAll();
	}

	public Optional<Client> findById(String id) {
		return clientRepository.findById(id);
	}

	public void save(Client client) {
		clientRepository.save(client);
	}

	public void update(Client client) {
		clientRepository.save(client);
	}

	public void deleteById(String id) {
		clientRepository.deleteById(id);
		;
	}
}
