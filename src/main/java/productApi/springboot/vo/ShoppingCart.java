package productApi.springboot.vo;

import java.util.ArrayList;
import java.util.List;

import productApi.springboot.model.Product;

public class ShoppingCart {

	private List<Product> products = new ArrayList<Product>();
	private double totalPrice = 0;

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String addProducts(Product p) {
		products.add(p);
		totalPrice = totalPrice + p.getPrice();
		return "Has agregado " + p.getName() + " y tu total a pagar actual es de: " + totalPrice;
	}

	public String deleteProduct(int i) {
		totalPrice = totalPrice - products.get(i).getPrice();
		products.remove(i);
		return products.toString() + " Total a pagar es de " + totalPrice;
	}

	public void emptyShopping() {
		products.clear();
	}

}
