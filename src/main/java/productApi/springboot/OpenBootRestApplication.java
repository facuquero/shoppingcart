package productApi.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenBootRestApplication {
	public static void main(String[] args) {
		SpringApplication.run(OpenBootRestApplication.class, args);
	}
}
