package productApi.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import productApi.springboot.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	public List<Transaction> findAllByCode(String code);
}
