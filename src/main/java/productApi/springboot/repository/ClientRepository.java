package productApi.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import productApi.springboot.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, String> {

}
