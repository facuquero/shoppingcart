package productApi.springboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import productApi.springboot.model.Product;
import productApi.springboot.service.ClientService;
import productApi.springboot.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {

	@Autowired
	ProductService productService;
	@Autowired
	ClientService clientService;

	@GetMapping("/products/all")
	public List<Product> getAllProducts() {
		return productService.getAllProducts();
	}

	@GetMapping("/products/{id}")
	public Optional<Product> findById(@PathVariable Long id) {
		return productService.findById(id);
	}

	@PostMapping("/products/save")
	public void save(@RequestBody Product product) {
		productService.save(product);
	}

	@PutMapping("/products/update")
	public void update(@RequestBody Product product) {
		productService.save(product);
	}

	@DeleteMapping("/products/{id}")
	public void deleteById(@PathVariable Long id) {
		productService.deleteById(id);
	}

	@GetMapping("/shopping")
	public String getShoppingCart() {
		return productService.getShoppingCart();
	}

	@GetMapping("/shopping/{id}")
	public String addShoppingCart(@PathVariable Long id) {
		return productService.addShoppingCart(id);
	}

	@DeleteMapping("/shopping/{position}")
	public String deleteShoppingCart(@PathVariable int position) {
		return productService.deleteShoppingCart(position);
	}

	@PutMapping("/shopping/buy/{clientId}")
	public String buy(@PathVariable String clientId) {
		return productService.buy(clientId);
	}
}
