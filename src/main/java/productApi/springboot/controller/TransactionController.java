package productApi.springboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import productApi.springboot.model.Transaction;
import productApi.springboot.service.TransactionService;

@RestController
@RequestMapping("/api")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	@GetMapping("/transactions/all")
	public List<Transaction> getAllProducts() {
		return transactionService.getAllProducts();
	}

	@GetMapping("/transactions/{id}")
	public Optional<Transaction> findById(@PathVariable Long id) {
		return transactionService.findById(id);
	}

	@PostMapping("/transactions/save")
	public void save(@RequestBody Transaction transaction) {
		transactionService.save(transaction);
	}

	@PutMapping("/transactions/update")
	public void update(@RequestBody Transaction transaction) {
		transactionService.save(transaction);
	}

	@DeleteMapping("/transactions/{id}")
	public void deleteById(@PathVariable Long id) {
		transactionService.deleteById(id);
	}
}
