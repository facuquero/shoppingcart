package productApi.springboot.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import productApi.springboot.model.Client;
import productApi.springboot.service.ClientService;

@RestController
@RequestMapping("/api")
public class ClientController {

	@Autowired
	ClientService clientService;

	@GetMapping("/clients")
	public List<Client> getAllClients() {
		return clientService.getAllClients();
	}

	@GetMapping("/clients/{id}")
	public Optional<Client> findById(@PathVariable String id) {
		return clientService.findById(id);
	}

	@PutMapping("/clients/save")
	public void save(@RequestBody Client client) {
		clientService.save(client);
	}

	@PutMapping("/clients/update")
	public void update(@RequestBody Client client) {
		clientService.save(client);
	}

	@DeleteMapping("/clients/{id}")
	public void deleteById(@PathVariable String id) {
		clientService.deleteById(id);
		;
	}

}
